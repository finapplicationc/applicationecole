﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppEcole
{
    public partial class PersoDetail : Form
    {
        Panel pp;
        public PersoDetail(Panel p)
        {
            InitializeComponent();
            this.pp = p;;
        }
        private void btnVal_Click(object sender, EventArgs e)
        {

            this.Close();
            btnSelec.Visible = false;

        }

        private void btnAnnu_Click(object sender, EventArgs e)
        {
            btnSelec.Visible = false;
            txtCin.Text = "";
            txtnom.Text = "";
            txtpre.Text = "";
            txtadr.Text = "";
            txttel.Text = "";
            txtvil.Text = "";
            txtcnss.Text = "";
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
           
            pp.SendToBack();
            this.Close();
        }

        private void PersoDetail_Load(object sender, EventArgs e)
        {
            btnSelec.Visible = false;
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            btnSelec.Visible = true;
        }
    }
}
