﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppEcole
{
    public partial class PersoFiche : Form
    {




        public PersoFiche()
        {
            InitializeComponent();
        }

        public static Form activeForm = null;


        public void openChildFormInPanel(Form childForm)
        {



            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }


        private void btnAjou_MouseEnter(object sender, EventArgs e)
        {
            btnAjou.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
            btnAjou.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnAjou_MouseLeave(object sender, EventArgs e)
        {
            btnAjou.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            btnAjou.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

   

        private void btnSup_MouseEnter(object sender, EventArgs e)
        {

            btnSup.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
            btnSup.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnSup_MouseLeave(object sender, EventArgs e)
        {
            btnSup.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            btnSup.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnVal_MouseLeave(object sender, EventArgs e)
        {
            btnVal.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            btnVal.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnVal_MouseEnter(object sender, EventArgs e)
        {

            btnVal.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
            btnVal.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnAnnu_MouseLeave(object sender, EventArgs e)
        {
            btnAnnu.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            btnAnnu.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnAnnu_MouseEnter(object sender, EventArgs e)
        {
            btnAnnu.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
            btnAnnu.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnCols_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCols_MouseEnter(object sender, EventArgs e)
        {
            btnCols.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
            btnCols.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnCols_MouseLeave(object sender, EventArgs e)
        {
            btnCols.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            btnCols.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnAjou_Click(object sender, EventArgs e)
        {

            panelChildForm.BringToFront();
            openChildFormInPanel(new PersoAjouter(panelChildForm));
        }

        private void bunifuCustomDataGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            panelChildForm.BringToFront();
            openChildFormInPanel(new PersoDetail(panelChildForm));
        }
    }
}
