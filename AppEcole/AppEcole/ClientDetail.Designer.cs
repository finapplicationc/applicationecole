﻿namespace AppEcole
{
    partial class ClientDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.iconButton14 = new FontAwesome.Sharp.IconButton();
            this.iconButton15 = new FontAwesome.Sharp.IconButton();
            this.iconButton16 = new FontAwesome.Sharp.IconButton();
            this.iconButton17 = new FontAwesome.Sharp.IconButton();
            this.iconButton18 = new FontAwesome.Sharp.IconButton();
            this.iconButton19 = new FontAwesome.Sharp.IconButton();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.iconButton8 = new FontAwesome.Sharp.IconButton();
            this.iconButton9 = new FontAwesome.Sharp.IconButton();
            this.iconButton10 = new FontAwesome.Sharp.IconButton();
            this.iconButton11 = new FontAwesome.Sharp.IconButton();
            this.iconButton12 = new FontAwesome.Sharp.IconButton();
            this.iconButton13 = new FontAwesome.Sharp.IconButton();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.iconButton2 = new FontAwesome.Sharp.IconButton();
            this.iconButton3 = new FontAwesome.Sharp.IconButton();
            this.iconButton4 = new FontAwesome.Sharp.IconButton();
            this.iconButton5 = new FontAwesome.Sharp.IconButton();
            this.iconButton6 = new FontAwesome.Sharp.IconButton();
            this.iconButton7 = new FontAwesome.Sharp.IconButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.btnVal = new FontAwesome.Sharp.IconButton();
            this.btnAnnu = new FontAwesome.Sharp.IconButton();
            this.btnCols = new FontAwesome.Sharp.IconButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtEma = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtpre = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtvil = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txttel = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtadr = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtCin = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.txtnom = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.rdEncou = new System.Windows.Forms.RadioButton();
            this.rdTerm = new System.Windows.Forms.RadioButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnSelec = new FontAwesome.Sharp.IconButton();
            this.rdFemme = new System.Windows.Forms.RadioButton();
            this.rdhomme = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuMetroTextbox1 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label10 = new System.Windows.Forms.Label();
            this.bunifuMetroTextbox2 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.metroDateTime2 = new MetroFramework.Controls.MetroDateTime();
            this.label8 = new System.Windows.Forms.Label();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.btnMod = new FontAwesome.Sharp.IconButton();
            this.iconButton21 = new FontAwesome.Sharp.IconButton();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(379, 9);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(112, 20);
            this.bunifuCustomLabel1.TabIndex = 20;
            this.bunifuCustomLabel1.Text = "Detail Client ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.btnCols);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(38, 562);
            this.panel2.TabIndex = 19;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.bunifuCustomLabel3);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Location = new System.Drawing.Point(340, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(121, 100);
            this.panel3.TabIndex = 21;
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.bunifuCustomLabel4);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Location = new System.Drawing.Point(381, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(121, 100);
            this.panel5.TabIndex = 21;
            // 
            // panel7
            // 
            this.panel7.BackgroundImage = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.bunifuCustomLabel5);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Location = new System.Drawing.Point(381, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(121, 100);
            this.panel7.TabIndex = 21;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel9.Location = new System.Drawing.Point(381, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(121, 100);
            this.panel9.TabIndex = 21;
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(-3, 9);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(129, 20);
            this.bunifuCustomLabel5.TabIndex = 20;
            this.bunifuCustomLabel5.Text = "Finance Client ";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel10.Controls.Add(this.iconButton14);
            this.panel10.Controls.Add(this.iconButton15);
            this.panel10.Controls.Add(this.iconButton16);
            this.panel10.Controls.Add(this.iconButton17);
            this.panel10.Controls.Add(this.iconButton18);
            this.panel10.Controls.Add(this.iconButton19);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(38, 100);
            this.panel10.TabIndex = 19;
            // 
            // iconButton14
            // 
            this.iconButton14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton14.FlatAppearance.BorderSize = 0;
            this.iconButton14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton14.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton14.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.iconButton14.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton14.IconSize = 35;
            this.iconButton14.Location = new System.Drawing.Point(0, 521);
            this.iconButton14.Name = "iconButton14";
            this.iconButton14.Rotation = 0D;
            this.iconButton14.Size = new System.Drawing.Size(38, 41);
            this.iconButton14.TabIndex = 14;
            this.iconButton14.UseVisualStyleBackColor = false;
            // 
            // iconButton15
            // 
            this.iconButton15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton15.FlatAppearance.BorderSize = 0;
            this.iconButton15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton15.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton15.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.iconButton15.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton15.IconSize = 35;
            this.iconButton15.Location = new System.Drawing.Point(0, 198);
            this.iconButton15.Name = "iconButton15";
            this.iconButton15.Rotation = 0D;
            this.iconButton15.Size = new System.Drawing.Size(38, 41);
            this.iconButton15.TabIndex = 13;
            this.iconButton15.UseVisualStyleBackColor = false;
            // 
            // iconButton16
            // 
            this.iconButton16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton16.FlatAppearance.BorderSize = 0;
            this.iconButton16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton16.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton16.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.iconButton16.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton16.IconSize = 35;
            this.iconButton16.Location = new System.Drawing.Point(0, 152);
            this.iconButton16.Name = "iconButton16";
            this.iconButton16.Rotation = 0D;
            this.iconButton16.Size = new System.Drawing.Size(38, 41);
            this.iconButton16.TabIndex = 10;
            this.iconButton16.UseVisualStyleBackColor = false;
            // 
            // iconButton17
            // 
            this.iconButton17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton17.FlatAppearance.BorderSize = 0;
            this.iconButton17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton17.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton17.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.iconButton17.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton17.IconSize = 35;
            this.iconButton17.Location = new System.Drawing.Point(0, 244);
            this.iconButton17.Name = "iconButton17";
            this.iconButton17.Rotation = 0D;
            this.iconButton17.Size = new System.Drawing.Size(38, 41);
            this.iconButton17.TabIndex = 12;
            this.iconButton17.UseVisualStyleBackColor = false;
            // 
            // iconButton18
            // 
            this.iconButton18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton18.FlatAppearance.BorderSize = 0;
            this.iconButton18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton18.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton18.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.iconButton18.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton18.IconSize = 35;
            this.iconButton18.Location = new System.Drawing.Point(0, 106);
            this.iconButton18.Name = "iconButton18";
            this.iconButton18.Rotation = 0D;
            this.iconButton18.Size = new System.Drawing.Size(38, 41);
            this.iconButton18.TabIndex = 9;
            this.iconButton18.UseVisualStyleBackColor = false;
            // 
            // iconButton19
            // 
            this.iconButton19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton19.FlatAppearance.BorderSize = 0;
            this.iconButton19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton19.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton19.IconChar = FontAwesome.Sharp.IconChar.UndoAlt;
            this.iconButton19.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton19.IconSize = 35;
            this.iconButton19.Location = new System.Drawing.Point(0, 290);
            this.iconButton19.Name = "iconButton19";
            this.iconButton19.Rotation = 0D;
            this.iconButton19.Size = new System.Drawing.Size(38, 41);
            this.iconButton19.TabIndex = 11;
            this.iconButton19.UseVisualStyleBackColor = false;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(-3, 9);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(129, 20);
            this.bunifuCustomLabel4.TabIndex = 20;
            this.bunifuCustomLabel4.Text = "Finance Client ";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel8.Controls.Add(this.iconButton8);
            this.panel8.Controls.Add(this.iconButton9);
            this.panel8.Controls.Add(this.iconButton10);
            this.panel8.Controls.Add(this.iconButton11);
            this.panel8.Controls.Add(this.iconButton12);
            this.panel8.Controls.Add(this.iconButton13);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(38, 100);
            this.panel8.TabIndex = 19;
            // 
            // iconButton8
            // 
            this.iconButton8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton8.FlatAppearance.BorderSize = 0;
            this.iconButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton8.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton8.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.iconButton8.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton8.IconSize = 35;
            this.iconButton8.Location = new System.Drawing.Point(0, 521);
            this.iconButton8.Name = "iconButton8";
            this.iconButton8.Rotation = 0D;
            this.iconButton8.Size = new System.Drawing.Size(38, 41);
            this.iconButton8.TabIndex = 14;
            this.iconButton8.UseVisualStyleBackColor = false;
            // 
            // iconButton9
            // 
            this.iconButton9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton9.FlatAppearance.BorderSize = 0;
            this.iconButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton9.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton9.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.iconButton9.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton9.IconSize = 35;
            this.iconButton9.Location = new System.Drawing.Point(0, 198);
            this.iconButton9.Name = "iconButton9";
            this.iconButton9.Rotation = 0D;
            this.iconButton9.Size = new System.Drawing.Size(38, 41);
            this.iconButton9.TabIndex = 13;
            this.iconButton9.UseVisualStyleBackColor = false;
            // 
            // iconButton10
            // 
            this.iconButton10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton10.FlatAppearance.BorderSize = 0;
            this.iconButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton10.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton10.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.iconButton10.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton10.IconSize = 35;
            this.iconButton10.Location = new System.Drawing.Point(0, 152);
            this.iconButton10.Name = "iconButton10";
            this.iconButton10.Rotation = 0D;
            this.iconButton10.Size = new System.Drawing.Size(38, 41);
            this.iconButton10.TabIndex = 10;
            this.iconButton10.UseVisualStyleBackColor = false;
            // 
            // iconButton11
            // 
            this.iconButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton11.FlatAppearance.BorderSize = 0;
            this.iconButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton11.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton11.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.iconButton11.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton11.IconSize = 35;
            this.iconButton11.Location = new System.Drawing.Point(0, 244);
            this.iconButton11.Name = "iconButton11";
            this.iconButton11.Rotation = 0D;
            this.iconButton11.Size = new System.Drawing.Size(38, 41);
            this.iconButton11.TabIndex = 12;
            this.iconButton11.UseVisualStyleBackColor = false;
            // 
            // iconButton12
            // 
            this.iconButton12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton12.FlatAppearance.BorderSize = 0;
            this.iconButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton12.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton12.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.iconButton12.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton12.IconSize = 35;
            this.iconButton12.Location = new System.Drawing.Point(0, 106);
            this.iconButton12.Name = "iconButton12";
            this.iconButton12.Rotation = 0D;
            this.iconButton12.Size = new System.Drawing.Size(38, 41);
            this.iconButton12.TabIndex = 9;
            this.iconButton12.UseVisualStyleBackColor = false;
            // 
            // iconButton13
            // 
            this.iconButton13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton13.FlatAppearance.BorderSize = 0;
            this.iconButton13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton13.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton13.IconChar = FontAwesome.Sharp.IconChar.UndoAlt;
            this.iconButton13.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton13.IconSize = 35;
            this.iconButton13.Location = new System.Drawing.Point(0, 290);
            this.iconButton13.Name = "iconButton13";
            this.iconButton13.Rotation = 0D;
            this.iconButton13.Size = new System.Drawing.Size(38, 41);
            this.iconButton13.TabIndex = 11;
            this.iconButton13.UseVisualStyleBackColor = false;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(-3, 9);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(129, 20);
            this.bunifuCustomLabel3.TabIndex = 20;
            this.bunifuCustomLabel3.Text = "Finance Client ";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel6.Controls.Add(this.dateTimePicker1);
            this.panel6.Controls.Add(this.iconButton2);
            this.panel6.Controls.Add(this.iconButton3);
            this.panel6.Controls.Add(this.iconButton4);
            this.panel6.Controls.Add(this.iconButton5);
            this.panel6.Controls.Add(this.iconButton6);
            this.panel6.Controls.Add(this.iconButton7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(38, 100);
            this.panel6.TabIndex = 19;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(0, 45);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 15;
            // 
            // iconButton2
            // 
            this.iconButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton2.FlatAppearance.BorderSize = 0;
            this.iconButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton2.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton2.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.iconButton2.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton2.IconSize = 35;
            this.iconButton2.Location = new System.Drawing.Point(0, 521);
            this.iconButton2.Name = "iconButton2";
            this.iconButton2.Rotation = 0D;
            this.iconButton2.Size = new System.Drawing.Size(38, 41);
            this.iconButton2.TabIndex = 14;
            this.iconButton2.UseVisualStyleBackColor = false;
            // 
            // iconButton3
            // 
            this.iconButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton3.FlatAppearance.BorderSize = 0;
            this.iconButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton3.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton3.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.iconButton3.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton3.IconSize = 35;
            this.iconButton3.Location = new System.Drawing.Point(0, 198);
            this.iconButton3.Name = "iconButton3";
            this.iconButton3.Rotation = 0D;
            this.iconButton3.Size = new System.Drawing.Size(38, 41);
            this.iconButton3.TabIndex = 13;
            this.iconButton3.UseVisualStyleBackColor = false;
            // 
            // iconButton4
            // 
            this.iconButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton4.FlatAppearance.BorderSize = 0;
            this.iconButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton4.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton4.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.iconButton4.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton4.IconSize = 35;
            this.iconButton4.Location = new System.Drawing.Point(0, 152);
            this.iconButton4.Name = "iconButton4";
            this.iconButton4.Rotation = 0D;
            this.iconButton4.Size = new System.Drawing.Size(38, 41);
            this.iconButton4.TabIndex = 10;
            this.iconButton4.UseVisualStyleBackColor = false;
            // 
            // iconButton5
            // 
            this.iconButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton5.FlatAppearance.BorderSize = 0;
            this.iconButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton5.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton5.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.iconButton5.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton5.IconSize = 35;
            this.iconButton5.Location = new System.Drawing.Point(0, 244);
            this.iconButton5.Name = "iconButton5";
            this.iconButton5.Rotation = 0D;
            this.iconButton5.Size = new System.Drawing.Size(38, 41);
            this.iconButton5.TabIndex = 12;
            this.iconButton5.UseVisualStyleBackColor = false;
            // 
            // iconButton6
            // 
            this.iconButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton6.FlatAppearance.BorderSize = 0;
            this.iconButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton6.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton6.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.iconButton6.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton6.IconSize = 35;
            this.iconButton6.Location = new System.Drawing.Point(0, 106);
            this.iconButton6.Name = "iconButton6";
            this.iconButton6.Rotation = 0D;
            this.iconButton6.Size = new System.Drawing.Size(38, 41);
            this.iconButton6.TabIndex = 9;
            this.iconButton6.UseVisualStyleBackColor = false;
            // 
            // iconButton7
            // 
            this.iconButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton7.FlatAppearance.BorderSize = 0;
            this.iconButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton7.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton7.IconChar = FontAwesome.Sharp.IconChar.UndoAlt;
            this.iconButton7.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton7.IconSize = 35;
            this.iconButton7.Location = new System.Drawing.Point(0, 290);
            this.iconButton7.Name = "iconButton7";
            this.iconButton7.Rotation = 0D;
            this.iconButton7.Size = new System.Drawing.Size(38, 41);
            this.iconButton7.TabIndex = 11;
            this.iconButton7.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel4.Controls.Add(this.iconButton21);
            this.panel4.Controls.Add(this.btnMod);
            this.panel4.Controls.Add(this.bunifuCustomLabel2);
            this.panel4.Controls.Add(this.iconButton1);
            this.panel4.Controls.Add(this.btnVal);
            this.panel4.Controls.Add(this.btnAnnu);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(38, 562);
            this.panel4.TabIndex = 19;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(-44, 21);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(129, 20);
            this.bunifuCustomLabel2.TabIndex = 20;
            this.bunifuCustomLabel2.Text = "Finance Client ";
            // 
            // iconButton1
            // 
            this.iconButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton1.FlatAppearance.BorderSize = 0;
            this.iconButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton1.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.iconButton1.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton1.IconSize = 35;
            this.iconButton1.Location = new System.Drawing.Point(0, 521);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Rotation = 0D;
            this.iconButton1.Size = new System.Drawing.Size(38, 41);
            this.iconButton1.TabIndex = 14;
            this.iconButton1.UseVisualStyleBackColor = false;
            this.iconButton1.Click += new System.EventHandler(this.iconButton1_Click);
            // 
            // btnVal
            // 
            this.btnVal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnVal.FlatAppearance.BorderSize = 0;
            this.btnVal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVal.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnVal.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.btnVal.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnVal.IconSize = 35;
            this.btnVal.Location = new System.Drawing.Point(0, 220);
            this.btnVal.Name = "btnVal";
            this.btnVal.Rotation = 0D;
            this.btnVal.Size = new System.Drawing.Size(38, 41);
            this.btnVal.TabIndex = 12;
            this.btnVal.UseVisualStyleBackColor = false;
            this.btnVal.Click += new System.EventHandler(this.btnVal_Click);
            // 
            // btnAnnu
            // 
            this.btnAnnu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnAnnu.FlatAppearance.BorderSize = 0;
            this.btnAnnu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnu.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnAnnu.IconChar = FontAwesome.Sharp.IconChar.UndoAlt;
            this.btnAnnu.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnAnnu.IconSize = 35;
            this.btnAnnu.Location = new System.Drawing.Point(0, 267);
            this.btnAnnu.Name = "btnAnnu";
            this.btnAnnu.Rotation = 0D;
            this.btnAnnu.Size = new System.Drawing.Size(38, 41);
            this.btnAnnu.TabIndex = 11;
            this.btnAnnu.UseVisualStyleBackColor = false;
            this.btnAnnu.Click += new System.EventHandler(this.btnAnnu_Click);
            // 
            // btnCols
            // 
            this.btnCols.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnCols.FlatAppearance.BorderSize = 0;
            this.btnCols.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCols.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnCols.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.btnCols.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnCols.IconSize = 35;
            this.btnCols.Location = new System.Drawing.Point(0, 521);
            this.btnCols.Name = "btnCols";
            this.btnCols.Rotation = 0D;
            this.btnCols.Size = new System.Drawing.Size(38, 41);
            this.btnCols.TabIndex = 14;
            this.btnCols.UseVisualStyleBackColor = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.bunifuMetroTextbox2);
            this.panel11.Controls.Add(this.label11);
            this.panel11.Controls.Add(this.bunifuMetroTextbox1);
            this.panel11.Controls.Add(this.label10);
            this.panel11.Controls.Add(this.label9);
            this.panel11.Controls.Add(this.label8);
            this.panel11.Controls.Add(this.metroDateTime2);
            this.panel11.Controls.Add(this.txtEma);
            this.panel11.Controls.Add(this.metroDateTime1);
            this.panel11.Controls.Add(this.txtpre);
            this.panel11.Controls.Add(this.txtvil);
            this.panel11.Controls.Add(this.txttel);
            this.panel11.Controls.Add(this.txtadr);
            this.panel11.Controls.Add(this.txtCin);
            this.panel11.Controls.Add(this.txtnom);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Controls.Add(this.label6);
            this.panel11.Controls.Add(this.label5);
            this.panel11.Controls.Add(this.label4);
            this.panel11.Controls.Add(this.label3);
            this.panel11.Controls.Add(this.label2);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Location = new System.Drawing.Point(61, 44);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(331, 506);
            this.panel11.TabIndex = 22;
            // 
            // txtEma
            // 
            this.txtEma.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtEma.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtEma.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtEma.BorderThickness = 2;
            this.txtEma.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEma.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtEma.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEma.isPassword = false;
            this.txtEma.Location = new System.Drawing.Point(134, 274);
            this.txtEma.Margin = new System.Windows.Forms.Padding(4);
            this.txtEma.Name = "txtEma";
            this.txtEma.Size = new System.Drawing.Size(168, 29);
            this.txtEma.TabIndex = 27;
            this.txtEma.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtpre
            // 
            this.txtpre.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtpre.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtpre.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtpre.BorderThickness = 2;
            this.txtpre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtpre.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtpre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtpre.isPassword = false;
            this.txtpre.Location = new System.Drawing.Point(134, 102);
            this.txtpre.Margin = new System.Windows.Forms.Padding(4);
            this.txtpre.Name = "txtpre";
            this.txtpre.Size = new System.Drawing.Size(168, 29);
            this.txtpre.TabIndex = 26;
            this.txtpre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtvil
            // 
            this.txtvil.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtvil.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtvil.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtvil.BorderThickness = 2;
            this.txtvil.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtvil.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtvil.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtvil.isPassword = false;
            this.txtvil.Location = new System.Drawing.Point(134, 231);
            this.txtvil.Margin = new System.Windows.Forms.Padding(4);
            this.txtvil.Name = "txtvil";
            this.txtvil.Size = new System.Drawing.Size(168, 29);
            this.txtvil.TabIndex = 25;
            this.txtvil.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txttel
            // 
            this.txttel.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txttel.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txttel.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txttel.BorderThickness = 2;
            this.txttel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txttel.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txttel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txttel.isPassword = false;
            this.txttel.Location = new System.Drawing.Point(134, 188);
            this.txttel.Margin = new System.Windows.Forms.Padding(4);
            this.txttel.Name = "txttel";
            this.txttel.Size = new System.Drawing.Size(168, 29);
            this.txttel.TabIndex = 24;
            this.txttel.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtadr
            // 
            this.txtadr.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtadr.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtadr.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtadr.BorderThickness = 2;
            this.txtadr.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtadr.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtadr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtadr.isPassword = false;
            this.txtadr.Location = new System.Drawing.Point(134, 145);
            this.txtadr.Margin = new System.Windows.Forms.Padding(4);
            this.txtadr.Name = "txtadr";
            this.txtadr.Size = new System.Drawing.Size(168, 29);
            this.txtadr.TabIndex = 23;
            this.txtadr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtCin
            // 
            this.txtCin.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtCin.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtCin.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtCin.BorderThickness = 2;
            this.txtCin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCin.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtCin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCin.isPassword = false;
            this.txtCin.Location = new System.Drawing.Point(134, 16);
            this.txtCin.Margin = new System.Windows.Forms.Padding(4);
            this.txtCin.Name = "txtCin";
            this.txtCin.Size = new System.Drawing.Size(168, 29);
            this.txtCin.TabIndex = 22;
            this.txtCin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtnom
            // 
            this.txtnom.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtnom.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtnom.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.txtnom.BorderThickness = 2;
            this.txtnom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtnom.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtnom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtnom.isPassword = false;
            this.txtnom.Location = new System.Drawing.Point(134, 59);
            this.txtnom.Margin = new System.Windows.Forms.Padding(4);
            this.txtnom.Name = "txtnom";
            this.txtnom.Size = new System.Drawing.Size(168, 29);
            this.txtnom.TabIndex = 16;
            this.txtnom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label7.Location = new System.Drawing.Point(3, 285);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Genre";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label6.Location = new System.Drawing.Point(3, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Ville";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label5.Location = new System.Drawing.Point(3, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Telephone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label4.Location = new System.Drawing.Point(3, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Adresse";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label3.Location = new System.Drawing.Point(3, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Prenom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label2.Location = new System.Drawing.Point(3, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "CIN";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.pictureBox4);
            this.panel12.Controls.Add(this.pictureBox5);
            this.panel12.Controls.Add(this.rdEncou);
            this.panel12.Controls.Add(this.rdTerm);
            this.panel12.Controls.Add(this.pictureBox3);
            this.panel12.Controls.Add(this.pictureBox2);
            this.panel12.Controls.Add(this.rdFemme);
            this.panel12.Controls.Add(this.rdhomme);
            this.panel12.Location = new System.Drawing.Point(398, 267);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(474, 277);
            this.panel12.TabIndex = 23;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::AppEcole.Properties.Resources.enco;
            this.pictureBox4.Location = new System.Drawing.Point(157, 174);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 33;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::AppEcole.Properties.Resources.Ok_50px;
            this.pictureBox5.Location = new System.Drawing.Point(43, 174);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            // 
            // rdEncou
            // 
            this.rdEncou.AutoSize = true;
            this.rdEncou.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdEncou.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.rdEncou.Location = new System.Drawing.Point(157, 242);
            this.rdEncou.Name = "rdEncou";
            this.rdEncou.Size = new System.Drawing.Size(76, 17);
            this.rdEncou.TabIndex = 31;
            this.rdEncou.TabStop = true;
            this.rdEncou.Text = "En coure";
            this.rdEncou.UseVisualStyleBackColor = true;
            // 
            // rdTerm
            // 
            this.rdTerm.AutoSize = true;
            this.rdTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTerm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.rdTerm.Location = new System.Drawing.Point(43, 242);
            this.rdTerm.Name = "rdTerm";
            this.rdTerm.Size = new System.Drawing.Size(74, 17);
            this.rdTerm.TabIndex = 30;
            this.rdTerm.TabStop = true;
            this.rdTerm.Text = "Terminer";
            this.rdTerm.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::AppEcole.Properties.Resources.womenred2;
            this.pictureBox3.Location = new System.Drawing.Point(157, 60);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 29;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::AppEcole.Properties.Resources.menred1;
            this.pictureBox2.Location = new System.Drawing.Point(43, 60);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 28;
            this.pictureBox2.TabStop = false;
            // 
            // btnSelec
            // 
            this.btnSelec.FlatAppearance.BorderSize = 0;
            this.btnSelec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelec.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnSelec.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelec.IconChar = FontAwesome.Sharp.IconChar.Images;
            this.btnSelec.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnSelec.IconSize = 40;
            this.btnSelec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelec.Location = new System.Drawing.Point(677, 223);
            this.btnSelec.Name = "btnSelec";
            this.btnSelec.Rotation = 0D;
            this.btnSelec.Size = new System.Drawing.Size(47, 38);
            this.btnSelec.TabIndex = 27;
            this.btnSelec.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelec.UseVisualStyleBackColor = true;
            // 
            // rdFemme
            // 
            this.rdFemme.AutoSize = true;
            this.rdFemme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdFemme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.rdFemme.Location = new System.Drawing.Point(157, 128);
            this.rdFemme.Name = "rdFemme";
            this.rdFemme.Size = new System.Drawing.Size(64, 17);
            this.rdFemme.TabIndex = 26;
            this.rdFemme.TabStop = true;
            this.rdFemme.Text = "Femme";
            this.rdFemme.UseVisualStyleBackColor = true;
            // 
            // rdhomme
            // 
            this.rdhomme.AutoSize = true;
            this.rdhomme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdhomme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.rdhomme.Location = new System.Drawing.Point(43, 128);
            this.rdhomme.Name = "rdhomme";
            this.rdhomme.Size = new System.Drawing.Size(66, 17);
            this.rdhomme.TabIndex = 25;
            this.rdhomme.TabStop = true;
            this.rdhomme.Text = "Homme";
            this.rdhomme.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AppEcole.Properties.Resources.menred2;
            this.pictureBox1.Location = new System.Drawing.Point(406, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(265, 217);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(763, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(121, 88);
            this.panel1.TabIndex = 21;
            // 
            // bunifuMetroTextbox1
            // 
            this.bunifuMetroTextbox1.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.bunifuMetroTextbox1.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuMetroTextbox1.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.bunifuMetroTextbox1.BorderThickness = 2;
            this.bunifuMetroTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMetroTextbox1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMetroTextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox1.isPassword = false;
            this.bunifuMetroTextbox1.Location = new System.Drawing.Point(134, 316);
            this.bunifuMetroTextbox1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMetroTextbox1.Name = "bunifuMetroTextbox1";
            this.bunifuMetroTextbox1.Size = new System.Drawing.Size(168, 29);
            this.bunifuMetroTextbox1.TabIndex = 31;
            this.bunifuMetroTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label10.Location = new System.Drawing.Point(3, 327);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Etat";
            // 
            // bunifuMetroTextbox2
            // 
            this.bunifuMetroTextbox2.BorderColorFocused = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.bunifuMetroTextbox2.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuMetroTextbox2.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.bunifuMetroTextbox2.BorderThickness = 2;
            this.bunifuMetroTextbox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMetroTextbox2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMetroTextbox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox2.isPassword = false;
            this.bunifuMetroTextbox2.Location = new System.Drawing.Point(134, 363);
            this.bunifuMetroTextbox2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMetroTextbox2.Name = "bunifuMetroTextbox2";
            this.bunifuMetroTextbox2.Size = new System.Drawing.Size(168, 29);
            this.bunifuMetroTextbox2.TabIndex = 33;
            this.bunifuMetroTextbox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label11.Location = new System.Drawing.Point(3, 374);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Photo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label9.Location = new System.Drawing.Point(3, 465);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Date d\'inscription";
            // 
            // metroDateTime2
            // 
            this.metroDateTime2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.metroDateTime2.Location = new System.Drawing.Point(134, 459);
            this.metroDateTime2.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime2.Name = "metroDateTime2";
            this.metroDateTime2.Size = new System.Drawing.Size(168, 29);
            this.metroDateTime2.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.label8.Location = new System.Drawing.Point(3, 422);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Date naissance";
            // 
            // metroDateTime1
            // 
            this.metroDateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.metroDateTime1.Location = new System.Drawing.Point(134, 415);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(168, 29);
            this.metroDateTime1.TabIndex = 0;
            // 
            // btnMod
            // 
            this.btnMod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnMod.FlatAppearance.BorderSize = 0;
            this.btnMod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMod.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnMod.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.btnMod.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnMod.IconSize = 35;
            this.btnMod.Location = new System.Drawing.Point(0, 172);
            this.btnMod.Name = "btnMod";
            this.btnMod.Rotation = 0D;
            this.btnMod.Size = new System.Drawing.Size(38, 41);
            this.btnMod.TabIndex = 21;
            this.btnMod.UseVisualStyleBackColor = false;
            this.btnMod.Click += new System.EventHandler(this.btnMod_Click);
            // 
            // iconButton21
            // 
            this.iconButton21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton21.FlatAppearance.BorderSize = 0;
            this.iconButton21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton21.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton21.IconChar = FontAwesome.Sharp.IconChar.Print;
            this.iconButton21.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton21.IconSize = 35;
            this.iconButton21.Location = new System.Drawing.Point(0, 306);
            this.iconButton21.Name = "iconButton21";
            this.iconButton21.Rotation = 0D;
            this.iconButton21.Size = new System.Drawing.Size(38, 41);
            this.iconButton21.TabIndex = 22;
            this.iconButton21.UseVisualStyleBackColor = false;
            // 
            // ClientDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSelec);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(444, 185);
            this.Name = "ClientDetail";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ClientDetail";
            this.Load += new System.EventHandler(this.ClientAjouter_Load);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private System.Windows.Forms.Panel panel10;
        private FontAwesome.Sharp.IconButton iconButton14;
        private FontAwesome.Sharp.IconButton iconButton15;
        private FontAwesome.Sharp.IconButton iconButton16;
        private FontAwesome.Sharp.IconButton iconButton17;
        private FontAwesome.Sharp.IconButton iconButton18;
        private FontAwesome.Sharp.IconButton iconButton19;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private System.Windows.Forms.Panel panel8;
        private FontAwesome.Sharp.IconButton iconButton8;
        private FontAwesome.Sharp.IconButton iconButton9;
        private FontAwesome.Sharp.IconButton iconButton10;
        private FontAwesome.Sharp.IconButton iconButton11;
        private FontAwesome.Sharp.IconButton iconButton12;
        private FontAwesome.Sharp.IconButton iconButton13;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private System.Windows.Forms.Panel panel6;
        private FontAwesome.Sharp.IconButton iconButton2;
        private FontAwesome.Sharp.IconButton iconButton3;
        private FontAwesome.Sharp.IconButton iconButton4;
        private FontAwesome.Sharp.IconButton iconButton5;
        private FontAwesome.Sharp.IconButton iconButton6;
        private FontAwesome.Sharp.IconButton iconButton7;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.Panel panel4;
        private FontAwesome.Sharp.IconButton iconButton1;
        private FontAwesome.Sharp.IconButton btnVal;
        private FontAwesome.Sharp.IconButton btnAnnu;
        private FontAwesome.Sharp.IconButton btnCols;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.RadioButton rdFemme;
        private System.Windows.Forms.RadioButton rdhomme;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.RadioButton rdEncou;
        private System.Windows.Forms.RadioButton rdTerm;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtnom;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtCin;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtEma;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtpre;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtvil;
        private Bunifu.Framework.UI.BunifuMetroTextbox txttel;
        private Bunifu.Framework.UI.BunifuMetroTextbox txtadr;
        private FontAwesome.Sharp.IconButton btnSelec;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox2;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private MetroFramework.Controls.MetroDateTime metroDateTime2;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private FontAwesome.Sharp.IconButton btnMod;
        private FontAwesome.Sharp.IconButton iconButton21;
    }
}