﻿namespace AppEcole
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnFermer = new FontAwesome.Sharp.IconButton();
            this.paneExa = new System.Windows.Forms.Panel();
            this.paneveh = new System.Windows.Forms.Panel();
            this.panePer = new System.Windows.Forms.Panel();
            this.panbtnCli = new System.Windows.Forms.Panel();
            this.panPer = new System.Windows.Forms.FlowLayoutPanel();
            this.btninf2 = new FontAwesome.Sharp.IconButton();
            this.btnfin2 = new FontAwesome.Sharp.IconButton();
            this.panCli = new System.Windows.Forms.FlowLayoutPanel();
            this.btninf1 = new FontAwesome.Sharp.IconButton();
            this.btnfin1 = new FontAwesome.Sharp.IconButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panmen = new System.Windows.Forms.Panel();
            this.panSta = new System.Windows.Forms.FlowLayoutPanel();
            this.btnIns = new FontAwesome.Sharp.IconButton();
            this.btnReu = new FontAwesome.Sharp.IconButton();
            this.btnEch = new FontAwesome.Sharp.IconButton();
            this.btnPro = new FontAwesome.Sharp.IconButton();
            this.paneSta = new System.Windows.Forms.Panel();
            this.btnDash = new FontAwesome.Sharp.IconButton();
            this.btnExa = new FontAwesome.Sharp.IconButton();
            this.btnVeh = new FontAwesome.Sharp.IconButton();
            this.btnPer = new FontAwesome.Sharp.IconButton();
            this.btnClie = new FontAwesome.Sharp.IconButton();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.btnFin3 = new FontAwesome.Sharp.IconButton();
            this.btninf3 = new FontAwesome.Sharp.IconButton();
            this.panVeh = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3.SuspendLayout();
            this.panPer.SuspendLayout();
            this.panCli.SuspendLayout();
            this.panmen.SuspendLayout();
            this.panSta.SuspendLayout();
            this.panelChildForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panVeh.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.panel3.Controls.Add(this.btnFermer);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.ForeColor = System.Drawing.Color.Coral;
            this.panel3.Location = new System.Drawing.Point(176, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(888, 78);
            this.panel3.TabIndex = 6;
            // 
            // btnFermer
            // 
            this.btnFermer.FlatAppearance.BorderSize = 0;
            this.btnFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFermer.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnFermer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFermer.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.btnFermer.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            this.btnFermer.IconSize = 35;
            this.btnFermer.Location = new System.Drawing.Point(847, 12);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Rotation = 0D;
            this.btnFermer.Size = new System.Drawing.Size(29, 34);
            this.btnFermer.TabIndex = 19;
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            this.btnFermer.MouseEnter += new System.EventHandler(this.btnFermer_MouseEnter);
            this.btnFermer.MouseLeave += new System.EventHandler(this.btnFermer_MouseLeave);
            // 
            // paneExa
            // 
            this.paneExa.Location = new System.Drawing.Point(3, 510);
            this.paneExa.Margin = new System.Windows.Forms.Padding(3, 50, 3, 3);
            this.paneExa.Name = "paneExa";
            this.paneExa.Size = new System.Drawing.Size(8, 39);
            this.paneExa.TabIndex = 17;
            // 
            // paneveh
            // 
            this.paneveh.Location = new System.Drawing.Point(3, 363);
            this.paneveh.Margin = new System.Windows.Forms.Padding(3, 50, 3, 3);
            this.paneveh.Name = "paneveh";
            this.paneveh.Size = new System.Drawing.Size(8, 39);
            this.paneveh.TabIndex = 14;
            // 
            // panePer
            // 
            this.panePer.Location = new System.Drawing.Point(0, 225);
            this.panePer.Margin = new System.Windows.Forms.Padding(3, 50, 3, 3);
            this.panePer.Name = "panePer";
            this.panePer.Size = new System.Drawing.Size(8, 39);
            this.panePer.TabIndex = 12;
            // 
            // panbtnCli
            // 
            this.panbtnCli.Location = new System.Drawing.Point(0, 78);
            this.panbtnCli.Name = "panbtnCli";
            this.panbtnCli.Size = new System.Drawing.Size(8, 39);
            this.panbtnCli.TabIndex = 11;
            // 
            // panPer
            // 
            this.panPer.Controls.Add(this.btninf2);
            this.panPer.Controls.Add(this.btnfin2);
            this.panPer.Dock = System.Windows.Forms.DockStyle.Top;
            this.panPer.Location = new System.Drawing.Point(0, 264);
            this.panPer.Name = "panPer";
            this.panPer.Size = new System.Drawing.Size(176, 99);
            this.panPer.TabIndex = 10;
            // 
            // btninf2
            // 
            this.btninf2.Dock = System.Windows.Forms.DockStyle.Top;
            this.btninf2.FlatAppearance.BorderSize = 0;
            this.btninf2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btninf2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btninf2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btninf2.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btninf2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninf2.ForeColor = System.Drawing.Color.White;
            this.btninf2.IconChar = FontAwesome.Sharp.IconChar.FolderOpen;
            this.btninf2.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btninf2.IconSize = 30;
            this.btninf2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btninf2.Location = new System.Drawing.Point(3, 3);
            this.btninf2.Name = "btninf2";
            this.btninf2.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.btninf2.Rotation = 0D;
            this.btninf2.Size = new System.Drawing.Size(171, 26);
            this.btninf2.TabIndex = 7;
            this.btninf2.Text = "Informations";
            this.btninf2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btninf2.UseVisualStyleBackColor = true;
            this.btninf2.Click += new System.EventHandler(this.btninf2_Click);
            // 
            // btnfin2
            // 
            this.btnfin2.FlatAppearance.BorderSize = 0;
            this.btnfin2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnfin2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnfin2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfin2.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnfin2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfin2.ForeColor = System.Drawing.Color.White;
            this.btnfin2.IconChar = FontAwesome.Sharp.IconChar.Coins;
            this.btnfin2.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnfin2.IconSize = 30;
            this.btnfin2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfin2.Location = new System.Drawing.Point(3, 35);
            this.btnfin2.Name = "btnfin2";
            this.btnfin2.Padding = new System.Windows.Forms.Padding(40, 0, 22, 0);
            this.btnfin2.Rotation = 0D;
            this.btnfin2.Size = new System.Drawing.Size(171, 26);
            this.btnfin2.TabIndex = 5;
            this.btnfin2.Text = "Finance ";
            this.btnfin2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfin2.UseVisualStyleBackColor = true;
            this.btnfin2.Click += new System.EventHandler(this.btnfin2_Click);
            // 
            // panCli
            // 
            this.panCli.Controls.Add(this.btninf1);
            this.panCli.Controls.Add(this.btnfin1);
            this.panCli.Dock = System.Windows.Forms.DockStyle.Top;
            this.panCli.Location = new System.Drawing.Point(0, 117);
            this.panCli.Name = "panCli";
            this.panCli.Size = new System.Drawing.Size(176, 108);
            this.panCli.TabIndex = 2;
            // 
            // btninf1
            // 
            this.btninf1.Dock = System.Windows.Forms.DockStyle.Top;
            this.btninf1.FlatAppearance.BorderSize = 0;
            this.btninf1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btninf1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btninf1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btninf1.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btninf1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninf1.ForeColor = System.Drawing.Color.White;
            this.btninf1.IconChar = FontAwesome.Sharp.IconChar.FolderOpen;
            this.btninf1.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btninf1.IconSize = 30;
            this.btninf1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btninf1.Location = new System.Drawing.Point(3, 3);
            this.btninf1.Name = "btninf1";
            this.btninf1.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.btninf1.Rotation = 0D;
            this.btninf1.Size = new System.Drawing.Size(171, 26);
            this.btninf1.TabIndex = 7;
            this.btninf1.Text = "Informations";
            this.btninf1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btninf1.UseVisualStyleBackColor = true;
            this.btninf1.Click += new System.EventHandler(this.btninf1_Click);
            // 
            // btnfin1
            // 
            this.btnfin1.FlatAppearance.BorderSize = 0;
            this.btnfin1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnfin1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnfin1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfin1.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnfin1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfin1.ForeColor = System.Drawing.Color.White;
            this.btnfin1.IconChar = FontAwesome.Sharp.IconChar.Coins;
            this.btnfin1.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnfin1.IconSize = 30;
            this.btnfin1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfin1.Location = new System.Drawing.Point(3, 35);
            this.btnfin1.Name = "btnfin1";
            this.btnfin1.Padding = new System.Windows.Forms.Padding(40, 0, 22, 0);
            this.btnfin1.Rotation = 0D;
            this.btnfin1.Size = new System.Drawing.Size(171, 26);
            this.btnfin1.TabIndex = 5;
            this.btnfin1.Text = "Finance ";
            this.btnfin1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfin1.UseVisualStyleBackColor = true;
            this.btnfin1.Click += new System.EventHandler(this.btnfin1_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(176, 78);
            this.panel1.TabIndex = 0;
            // 
            // panmen
            // 
            this.panmen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.panmen.Controls.Add(this.panSta);
            this.panmen.Controls.Add(this.paneSta);
            this.panmen.Controls.Add(this.btnDash);
            this.panmen.Controls.Add(this.paneExa);
            this.panmen.Controls.Add(this.btnExa);
            this.panmen.Controls.Add(this.panVeh);
            this.panmen.Controls.Add(this.paneveh);
            this.panmen.Controls.Add(this.btnVeh);
            this.panmen.Controls.Add(this.panePer);
            this.panmen.Controls.Add(this.panbtnCli);
            this.panmen.Controls.Add(this.panPer);
            this.panmen.Controls.Add(this.btnPer);
            this.panmen.Controls.Add(this.panCli);
            this.panmen.Controls.Add(this.btnClie);
            this.panmen.Controls.Add(this.panel1);
            this.panmen.Dock = System.Windows.Forms.DockStyle.Left;
            this.panmen.Location = new System.Drawing.Point(0, 0);
            this.panmen.Name = "panmen";
            this.panmen.Size = new System.Drawing.Size(176, 647);
            this.panmen.TabIndex = 5;
            // 
            // panSta
            // 
            this.panSta.Controls.Add(this.btnIns);
            this.panSta.Controls.Add(this.btnReu);
            this.panSta.Controls.Add(this.btnEch);
            this.panSta.Controls.Add(this.btnPro);
            this.panSta.Dock = System.Windows.Forms.DockStyle.Top;
            this.panSta.Location = new System.Drawing.Point(0, 588);
            this.panSta.Name = "panSta";
            this.panSta.Size = new System.Drawing.Size(176, 164);
            this.panSta.TabIndex = 20;
            // 
            // btnIns
            // 
            this.btnIns.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIns.FlatAppearance.BorderSize = 0;
            this.btnIns.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnIns.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnIns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIns.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnIns.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIns.ForeColor = System.Drawing.Color.White;
            this.btnIns.IconChar = FontAwesome.Sharp.IconChar.FolderOpen;
            this.btnIns.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnIns.IconSize = 30;
            this.btnIns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIns.Location = new System.Drawing.Point(3, 3);
            this.btnIns.Name = "btnIns";
            this.btnIns.Padding = new System.Windows.Forms.Padding(40, 0, 14, 0);
            this.btnIns.Rotation = 0D;
            this.btnIns.Size = new System.Drawing.Size(171, 26);
            this.btnIns.TabIndex = 7;
            this.btnIns.Text = "Inscription";
            this.btnIns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIns.UseVisualStyleBackColor = true;
            this.btnIns.Click += new System.EventHandler(this.btnIns_Click);
            // 
            // btnReu
            // 
            this.btnReu.FlatAppearance.BorderSize = 0;
            this.btnReu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnReu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnReu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReu.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnReu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReu.ForeColor = System.Drawing.Color.White;
            this.btnReu.IconChar = FontAwesome.Sharp.IconChar.ThumbsUp;
            this.btnReu.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnReu.IconSize = 30;
            this.btnReu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReu.Location = new System.Drawing.Point(3, 35);
            this.btnReu.Name = "btnReu";
            this.btnReu.Padding = new System.Windows.Forms.Padding(40, 0, 22, 0);
            this.btnReu.Rotation = 0D;
            this.btnReu.Size = new System.Drawing.Size(171, 26);
            this.btnReu.TabIndex = 5;
            this.btnReu.Text = "Réussite";
            this.btnReu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReu.UseVisualStyleBackColor = true;
            this.btnReu.Click += new System.EventHandler(this.btnReu_Click);
            // 
            // btnEch
            // 
            this.btnEch.FlatAppearance.BorderSize = 0;
            this.btnEch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnEch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnEch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEch.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnEch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEch.ForeColor = System.Drawing.Color.White;
            this.btnEch.IconChar = FontAwesome.Sharp.IconChar.ThumbsDown;
            this.btnEch.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnEch.IconSize = 30;
            this.btnEch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEch.Location = new System.Drawing.Point(3, 67);
            this.btnEch.Name = "btnEch";
            this.btnEch.Padding = new System.Windows.Forms.Padding(40, 0, 30, 0);
            this.btnEch.Rotation = 0D;
            this.btnEch.Size = new System.Drawing.Size(168, 26);
            this.btnEch.TabIndex = 6;
            this.btnEch.Text = "Echec";
            this.btnEch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEch.UseVisualStyleBackColor = true;
            this.btnEch.Click += new System.EventHandler(this.btnEch_Click);
            // 
            // btnPro
            // 
            this.btnPro.FlatAppearance.BorderSize = 0;
            this.btnPro.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnPro.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnPro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPro.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnPro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPro.ForeColor = System.Drawing.Color.White;
            this.btnPro.IconChar = FontAwesome.Sharp.IconChar.HandHoldingUsd;
            this.btnPro.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnPro.IconSize = 30;
            this.btnPro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPro.Location = new System.Drawing.Point(3, 99);
            this.btnPro.Name = "btnPro";
            this.btnPro.Padding = new System.Windows.Forms.Padding(40, 0, 35, 0);
            this.btnPro.Rotation = 0D;
            this.btnPro.Size = new System.Drawing.Size(168, 26);
            this.btnPro.TabIndex = 8;
            this.btnPro.Text = "Profit";
            this.btnPro.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPro.UseVisualStyleBackColor = true;
            this.btnPro.Click += new System.EventHandler(this.btnPro_Click);
            // 
            // paneSta
            // 
            this.paneSta.Location = new System.Drawing.Point(3, 549);
            this.paneSta.Margin = new System.Windows.Forms.Padding(3, 50, 3, 3);
            this.paneSta.Name = "paneSta";
            this.paneSta.Size = new System.Drawing.Size(8, 39);
            this.paneSta.TabIndex = 19;
            // 
            // btnDash
            // 
            this.btnDash.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDash.FlatAppearance.BorderSize = 0;
            this.btnDash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDash.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnDash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDash.ForeColor = System.Drawing.Color.White;
            this.btnDash.IconChar = FontAwesome.Sharp.IconChar.ChartLine;
            this.btnDash.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnDash.IconSize = 40;
            this.btnDash.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDash.Location = new System.Drawing.Point(0, 549);
            this.btnDash.Name = "btnDash";
            this.btnDash.Padding = new System.Windows.Forms.Padding(5, 0, 15, 0);
            this.btnDash.Rotation = 0D;
            this.btnDash.Size = new System.Drawing.Size(176, 39);
            this.btnDash.TabIndex = 18;
            this.btnDash.Text = "Statistique";
            this.btnDash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDash.UseVisualStyleBackColor = true;
            this.btnDash.Click += new System.EventHandler(this.btnDash_Click);
            this.btnDash.MouseEnter += new System.EventHandler(this.btnDash_MouseEnter);
            this.btnDash.MouseLeave += new System.EventHandler(this.btnDash_MouseLeave);
            // 
            // btnExa
            // 
            this.btnExa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnExa.FlatAppearance.BorderSize = 0;
            this.btnExa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExa.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnExa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExa.ForeColor = System.Drawing.Color.White;
            this.btnExa.IconChar = FontAwesome.Sharp.IconChar.PencilAlt;
            this.btnExa.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnExa.IconSize = 40;
            this.btnExa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExa.Location = new System.Drawing.Point(0, 510);
            this.btnExa.Name = "btnExa";
            this.btnExa.Padding = new System.Windows.Forms.Padding(5, 0, 30, 0);
            this.btnExa.Rotation = 0D;
            this.btnExa.Size = new System.Drawing.Size(176, 39);
            this.btnExa.TabIndex = 16;
            this.btnExa.Text = "Examen ";
            this.btnExa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExa.UseVisualStyleBackColor = true;
            this.btnExa.Click += new System.EventHandler(this.btnExa_Click);
            this.btnExa.MouseEnter += new System.EventHandler(this.btnExa_MouseEnter);
            this.btnExa.MouseLeave += new System.EventHandler(this.btnExa_MouseLeave);
            // 
            // btnVeh
            // 
            this.btnVeh.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVeh.FlatAppearance.BorderSize = 0;
            this.btnVeh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVeh.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnVeh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVeh.ForeColor = System.Drawing.Color.White;
            this.btnVeh.IconChar = FontAwesome.Sharp.IconChar.Car;
            this.btnVeh.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnVeh.IconSize = 40;
            this.btnVeh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVeh.Location = new System.Drawing.Point(0, 363);
            this.btnVeh.Name = "btnVeh";
            this.btnVeh.Padding = new System.Windows.Forms.Padding(5, 0, 30, 0);
            this.btnVeh.Rotation = 0D;
            this.btnVeh.Size = new System.Drawing.Size(176, 39);
            this.btnVeh.TabIndex = 13;
            this.btnVeh.Text = "Véhicule";
            this.btnVeh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVeh.UseVisualStyleBackColor = true;
            this.btnVeh.Click += new System.EventHandler(this.btnVeh_Click);
            this.btnVeh.MouseEnter += new System.EventHandler(this.btnVeh_MouseEnter);
            this.btnVeh.MouseLeave += new System.EventHandler(this.btnVeh_MouseLeave);
            // 
            // btnPer
            // 
            this.btnPer.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPer.FlatAppearance.BorderSize = 0;
            this.btnPer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPer.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnPer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPer.ForeColor = System.Drawing.Color.White;
            this.btnPer.IconChar = FontAwesome.Sharp.IconChar.UserTie;
            this.btnPer.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnPer.IconSize = 40;
            this.btnPer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPer.Location = new System.Drawing.Point(0, 225);
            this.btnPer.Name = "btnPer";
            this.btnPer.Padding = new System.Windows.Forms.Padding(0, 0, 18, 0);
            this.btnPer.Rotation = 0D;
            this.btnPer.Size = new System.Drawing.Size(176, 39);
            this.btnPer.TabIndex = 9;
            this.btnPer.Text = "Personnel";
            this.btnPer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPer.UseVisualStyleBackColor = true;
            this.btnPer.Click += new System.EventHandler(this.btnPer_Click);
            this.btnPer.MouseEnter += new System.EventHandler(this.btnPer_MouseEnter);
            this.btnPer.MouseLeave += new System.EventHandler(this.btnPer_MouseLeave);
            // 
            // btnClie
            // 
            this.btnClie.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnClie.FlatAppearance.BorderSize = 0;
            this.btnClie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClie.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnClie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClie.ForeColor = System.Drawing.Color.White;
            this.btnClie.IconChar = FontAwesome.Sharp.IconChar.UserFriends;
            this.btnClie.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnClie.IconSize = 40;
            this.btnClie.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClie.Location = new System.Drawing.Point(0, 78);
            this.btnClie.Name = "btnClie";
            this.btnClie.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnClie.Rotation = 0D;
            this.btnClie.Size = new System.Drawing.Size(176, 39);
            this.btnClie.TabIndex = 1;
            this.btnClie.Text = "Client";
            this.btnClie.UseVisualStyleBackColor = true;
            this.btnClie.Click += new System.EventHandler(this.btnClie_Click);
            this.btnClie.MouseEnter += new System.EventHandler(this.btnClie_MouseEnter);
            this.btnClie.MouseLeave += new System.EventHandler(this.btnClie_MouseLeave);
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.SystemColors.Control;
            this.panelChildForm.Controls.Add(this.pictureBox1);
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(176, 78);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(888, 569);
            this.panelChildForm.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Image = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.pictureBox1.Location = new System.Drawing.Point(264, 106);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(323, 308);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.panel3;
            this.bunifuDragControl1.Vertical = true;
            // 
            // btnFin3
            // 
            this.btnFin3.FlatAppearance.BorderSize = 0;
            this.btnFin3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnFin3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnFin3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFin3.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnFin3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFin3.ForeColor = System.Drawing.Color.White;
            this.btnFin3.IconChar = FontAwesome.Sharp.IconChar.Coins;
            this.btnFin3.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnFin3.IconSize = 30;
            this.btnFin3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFin3.Location = new System.Drawing.Point(3, 35);
            this.btnFin3.Name = "btnFin3";
            this.btnFin3.Padding = new System.Windows.Forms.Padding(40, 0, 22, 0);
            this.btnFin3.Rotation = 0D;
            this.btnFin3.Size = new System.Drawing.Size(171, 26);
            this.btnFin3.TabIndex = 5;
            this.btnFin3.Text = "Finance ";
            this.btnFin3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFin3.UseVisualStyleBackColor = true;
            this.btnFin3.Click += new System.EventHandler(this.iconButton3_Click);
            // 
            // btninf3
            // 
            this.btninf3.Dock = System.Windows.Forms.DockStyle.Top;
            this.btninf3.FlatAppearance.BorderSize = 0;
            this.btninf3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btninf3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btninf3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btninf3.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btninf3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninf3.ForeColor = System.Drawing.Color.White;
            this.btninf3.IconChar = FontAwesome.Sharp.IconChar.FolderOpen;
            this.btninf3.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btninf3.IconSize = 30;
            this.btninf3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btninf3.Location = new System.Drawing.Point(3, 3);
            this.btninf3.Name = "btninf3";
            this.btninf3.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.btninf3.Rotation = 0D;
            this.btninf3.Size = new System.Drawing.Size(171, 26);
            this.btninf3.TabIndex = 7;
            this.btninf3.Text = "Informations";
            this.btninf3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btninf3.UseVisualStyleBackColor = true;
            this.btninf3.Click += new System.EventHandler(this.iconButton2_Click);
            // 
            // panVeh
            // 
            this.panVeh.Controls.Add(this.btninf3);
            this.panVeh.Controls.Add(this.btnFin3);
            this.panVeh.Dock = System.Windows.Forms.DockStyle.Top;
            this.panVeh.Location = new System.Drawing.Point(0, 402);
            this.panVeh.Name = "panVeh";
            this.panVeh.Size = new System.Drawing.Size(176, 108);
            this.panVeh.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1064, 647);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panmen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel3.ResumeLayout(false);
            this.panPer.ResumeLayout(false);
            this.panCli.ResumeLayout(false);
            this.panmen.ResumeLayout(false);
            this.panSta.ResumeLayout(false);
            this.panelChildForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panVeh.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel paneExa;
        private FontAwesome.Sharp.IconButton btnExa;
        private FontAwesome.Sharp.IconButton btninf1;
        private FontAwesome.Sharp.IconButton btnfin1;
        private System.Windows.Forms.Panel paneveh;
        private FontAwesome.Sharp.IconButton btnVeh;
        private System.Windows.Forms.Panel panePer;
        private System.Windows.Forms.Panel panbtnCli;
        private FontAwesome.Sharp.IconButton btnfin2;
        private System.Windows.Forms.FlowLayoutPanel panPer;
        private FontAwesome.Sharp.IconButton btninf2;
        private FontAwesome.Sharp.IconButton btnPer;
        private System.Windows.Forms.FlowLayoutPanel panCli;
        private FontAwesome.Sharp.IconButton btnClie;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panmen;
        private System.Windows.Forms.FlowLayoutPanel panSta;
        private FontAwesome.Sharp.IconButton btnIns;
        private FontAwesome.Sharp.IconButton btnReu;
        private FontAwesome.Sharp.IconButton btnEch;
        private System.Windows.Forms.Panel paneSta;
        private FontAwesome.Sharp.IconButton btnDash;
        private FontAwesome.Sharp.IconButton btnPro;
        private System.Windows.Forms.Panel panelChildForm;
        private System.Windows.Forms.PictureBox pictureBox1;
        private FontAwesome.Sharp.IconButton btnFermer;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.FlowLayoutPanel panVeh;
        private FontAwesome.Sharp.IconButton btninf3;
        private FontAwesome.Sharp.IconButton btnFin3;
    }
}

