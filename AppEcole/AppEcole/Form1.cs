﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using System.Runtime.InteropServices;

namespace AppEcole
{
    public  partial class Form1 : Form
    {

        public static Form activeForm = null;


        public  void openChildFormInPanel(Form childForm)
        {



            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }


        private IconButton currentBtn;
        //RaduisForm 
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
          private static extern IntPtr CreateRoundRectRgn(
           int nLeftRect,
           int nTopRect,
           int nRightRect,
           int nRightBottom,
           int nwidthEllip,
           int nHeightEllip

           );

        
        

        //visiblPanel
        private void Customisdis()
        {
            panCli.Visible = false;
            panPer.Visible = false;
            panVeh.Visible = false;
        }


        //ActiverButton
        private void ActivateButton(object senderBtn)
        {
            if (senderBtn != null)
            {
                DisableButton();
                //Button
                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(52, 52, 52);
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = Color.FromArgb(182, 27, 27);
                currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;


            }
        }






        //DesactiverButton 
        private void DisableButton()
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.FromArgb(52, 52, 52);
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = Color.FromArgb(186, 27, 27);
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }


        //HidePAanel
        private void hidebutton()
        {

            if (panCli.Visible == true)
            {
                panCli.Visible = false;

            }
            if (panPer.Visible == true)
            {
                panPer.Visible = false;

            }
            if (panVeh.Visible == true)
            {
                panVeh.Visible = false;
            }

            if (panSta.Visible == true)
            {
                panSta.Visible = false;
            }
        }


        //ShowPAnel
        private void showbutton(Panel panMen)
        {
            if (panMen.Visible == false)
            {
                hidebutton();
                panMen.Visible = true;
            }
            else
                panMen.Visible = false;

        }


      


        //InitialiseForm
        public Form1()
        {
            InitializeComponent();
            this.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            Customisdis();
            
        }

      

        private void Form1_Load(object sender, EventArgs e)
        {
            hidebutton();
            panbtnCli.Visible = false;
        }


        
        private void btninf1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
           openChildFormInPanel(new ClientFiche());
         

        }

        private void btnfin1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            openChildFormInPanel(new ClientFinance());
        }

        private void btnexm1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
        }

        private void btninf2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
           openChildFormInPanel(new PersoFiche());
        }

        private void btnfin2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            openChildFormInPanel(new PersoFinance() );
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            openChildFormInPanel(new VehFiche());
        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
          openChildFormInPanel(new VehFinance());
        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
        }

        private void btnIns_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            openChildFormInPanel(new StatInscription());

        }

        private void btnReu_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            openChildFormInPanel(new StatReussi());
        }

        private void btnEch_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            openChildFormInPanel(new StatEchec());
        }

        private void btnPro_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);

        }




        private void btnClie_MouseEnter(object sender, EventArgs e)
        {
            this.btnClie.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnClie.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnClie_MouseLeave(object sender, EventArgs e)
        {
            this.btnClie.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnClie.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnPer_MouseEnter(object sender, EventArgs e)
        {
            this.btnPer.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnPer.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnPer_MouseLeave(object sender, EventArgs e)
        {
            this.btnPer.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnPer.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnVeh_MouseEnter(object sender, EventArgs e)
        {
            this.btnVeh.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnVeh.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnVeh_MouseLeave(object sender, EventArgs e)
        {
            this.btnVeh.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnVeh.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnExa_MouseEnter(object sender, EventArgs e)
        {
            this.btnExa.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnExa.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnExa_MouseLeave(object sender, EventArgs e)
        {
            this.btnExa.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnExa.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnDash_MouseEnter(object sender, EventArgs e)
        {
            this.btnDash.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnDash.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnDash_MouseLeave(object sender, EventArgs e)
        {
            this.btnDash.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnDash.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

       
        

        private void CloseIcon_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void btnClie_Click(object sender, EventArgs e)
        {
           
            if (panCli.Visible == false)
            {
                ActivateButton(sender);
                showbutton(panCli);
                panbtnCli.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
                panePer.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
                panbtnCli.Visible = true;
                paneExa.Visible = false;
                paneveh.Visible = false;
                panePer.Visible = false;
                paneSta.Visible = false;
            }
            else
            {
                DisableButton();
                panCli.Visible = false;
            }
        }

        private void btnPer_Click(object sender, EventArgs e)

        {

            if (panPer.Visible == false)
            {
                ActivateButton(sender);
                showbutton(panPer);
                panePer.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
                panePer.Location = new System.Drawing.Point(0, 117);
                panePer.Visible = true;
                panbtnCli.Visible = false;
                paneveh.Visible = false;
                paneExa.Visible = false;
                paneSta.Visible = false;
            }
            else
            {
                DisableButton();
                panPer.Visible = false;
            }

        }

        private void btnVeh_Click(object sender, EventArgs e)
        {
            if (panVeh.Visible == false)
            {
                
                ActivateButton(sender);
                showbutton(panVeh);
                paneveh.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
                paneveh.Location = new System.Drawing.Point(0, 156);
                paneveh.Visible = true;
                panbtnCli.Visible = false;
                panePer.Visible = false;
                paneExa.Visible = false;
                paneSta.Visible = false;

            }
            else
            {
                DisableButton();
                panVeh.Visible = false;
            }
        }

        private void btnExa_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            paneExa.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            paneExa.Location = new System.Drawing.Point(0, 195);
            paneExa.Visible = true;
            paneveh.Visible = false;
            panbtnCli.Visible = false;
            panePer.Visible = false;
            paneSta.Visible = false;


            panCli.Visible = false;
            panPer.Visible = false;
            panVeh.Visible = false;
            panSta.Visible = false;

            openChildFormInPanel(new Exam());
        }



        private void btnDash_Click(object sender, EventArgs e)
        {
            if (panSta.Visible == false)
            {
                paneSta.Visible = true;
                ActivateButton(sender);
                showbutton(panSta);
                paneSta.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
                paneSta.Location = new System.Drawing.Point(0, 234);
                panbtnCli.Visible = false;
                panePer.Visible = false;
                paneExa.Visible = false;
                paneveh.Visible = false;

            }

            else
            {
                DisableButton();
                panSta.Visible = false;
            }
        }

        private void btnFermer_MouseEnter(object sender, EventArgs e)
        {
             this.btnFermer.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnFermer_MouseLeave(object sender, EventArgs e)
        {
            this.btnFermer.IconColor = System.Drawing.Color.FromArgb(82,82,82);
          
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    }

