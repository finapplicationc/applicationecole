﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AppEcole
{
    public partial class Login : Form
    {
        //RaduisForm 
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn(
           int nLeftRect,
           int nTopRect,
           int nRightRect,
           int nRightBottom,
           int nwidthEllip,
           int nHeightEllip

           );

        public Login()
        {
            InitializeComponent();
            this.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 15, 15));
            txtpass.Focus();
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            
        }

        private const int cGrip = 16;
        private const int cCaption=32;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg==0x84)
            {
                Point pos = new Point(m.LParam.ToInt32());
                pos = this.PointToClient(pos);
                if (pos.Y < cCaption)
                {
                    m.Result = (IntPtr)2;
                }

                if(pos.X >= this.ClientSize.Width - cGrip && pos.Y >= this.ClientSize.Height - cGrip)
                {
                    m.Result = (IntPtr)17;
                    return;
                }
            }
            base.WndProc(ref m);
        }

   

        private void closeLogin_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            
        }

    

        private void btnConnecter_MouseEnter(object sender, EventArgs e)
        {
            this.btnConnecter.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnConnecter.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnConnecter_MouseLeave(object sender, EventArgs e)
        {

            this.btnConnecter.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btnConnecter.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
         
        }

        private void btninsci_MouseEnter(object sender, EventArgs e)
        {
            this.btninsci.BackColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btninsci.IconColor = System.Drawing.Color.FromArgb(52, 52, 52);
            
        }

        private void btninsci_MouseLeave(object sender, EventArgs e)
        {
            this.btninsci.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
            this.btninsci.BackColor = System.Drawing.Color.FromArgb(52, 52, 52);
        }

        private void btnConnecter_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.ShowDialog();
        }

        private void btnFermer_MouseHover(object sender, EventArgs e)
        {

        }

        private void btnFermer_MouseEnter(object sender, EventArgs e)
        {
            this.btnFermer.IconColor = System.Drawing.Color.FromArgb(186, 27, 27);
        }

        private void btnFermer_MouseLeave(object sender, EventArgs e)
        {
            this.btnFermer.IconColor = System.Drawing.Color.FromArgb(52,52,52);
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
