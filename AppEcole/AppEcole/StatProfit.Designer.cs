﻿namespace AppEcole
{
    partial class StatProfit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.iconButton21 = new FontAwesome.Sharp.IconButton();
            this.btnCols = new FontAwesome.Sharp.IconButton();
            this.btnSup = new FontAwesome.Sharp.IconButton();
            this.btnMod = new FontAwesome.Sharp.IconButton();
            this.btnVal = new FontAwesome.Sharp.IconButton();
            this.btnAjou = new FontAwesome.Sharp.IconButton();
            this.btnAnnu = new FontAwesome.Sharp.IconButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel2.Controls.Add(this.iconButton21);
            this.panel2.Controls.Add(this.btnCols);
            this.panel2.Controls.Add(this.btnSup);
            this.panel2.Controls.Add(this.btnMod);
            this.panel2.Controls.Add(this.btnVal);
            this.panel2.Controls.Add(this.btnAjou);
            this.panel2.Controls.Add(this.btnAnnu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(38, 562);
            this.panel2.TabIndex = 13;
            // 
            // iconButton21
            // 
            this.iconButton21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.iconButton21.FlatAppearance.BorderSize = 0;
            this.iconButton21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton21.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton21.IconChar = FontAwesome.Sharp.IconChar.Print;
            this.iconButton21.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.iconButton21.IconSize = 35;
            this.iconButton21.Location = new System.Drawing.Point(0, 328);
            this.iconButton21.Name = "iconButton21";
            this.iconButton21.Rotation = 0D;
            this.iconButton21.Size = new System.Drawing.Size(38, 41);
            this.iconButton21.TabIndex = 17;
            this.iconButton21.UseVisualStyleBackColor = false;
            // 
            // btnCols
            // 
            this.btnCols.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnCols.FlatAppearance.BorderSize = 0;
            this.btnCols.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCols.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnCols.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.btnCols.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnCols.IconSize = 35;
            this.btnCols.Location = new System.Drawing.Point(0, 521);
            this.btnCols.Name = "btnCols";
            this.btnCols.Rotation = 0D;
            this.btnCols.Size = new System.Drawing.Size(38, 41);
            this.btnCols.TabIndex = 14;
            this.btnCols.UseVisualStyleBackColor = false;
            this.btnCols.Click += new System.EventHandler(this.btnCols_Click);
            this.btnCols.MouseEnter += new System.EventHandler(this.btnCols_MouseEnter);
            this.btnCols.MouseLeave += new System.EventHandler(this.btnCols_MouseLeave);
            // 
            // btnSup
            // 
            this.btnSup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnSup.FlatAppearance.BorderSize = 0;
            this.btnSup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSup.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnSup.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.btnSup.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnSup.IconSize = 35;
            this.btnSup.Location = new System.Drawing.Point(0, 198);
            this.btnSup.Name = "btnSup";
            this.btnSup.Rotation = 0D;
            this.btnSup.Size = new System.Drawing.Size(38, 41);
            this.btnSup.TabIndex = 13;
            this.btnSup.UseVisualStyleBackColor = false;
            this.btnSup.MouseEnter += new System.EventHandler(this.btnSup_MouseEnter);
            this.btnSup.MouseLeave += new System.EventHandler(this.btnSup_MouseLeave);
            // 
            // btnMod
            // 
            this.btnMod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnMod.FlatAppearance.BorderSize = 0;
            this.btnMod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMod.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnMod.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.btnMod.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnMod.IconSize = 35;
            this.btnMod.Location = new System.Drawing.Point(0, 152);
            this.btnMod.Name = "btnMod";
            this.btnMod.Rotation = 0D;
            this.btnMod.Size = new System.Drawing.Size(38, 41);
            this.btnMod.TabIndex = 10;
            this.btnMod.UseVisualStyleBackColor = false;
            this.btnMod.MouseEnter += new System.EventHandler(this.btnMod_MouseEnter);
            this.btnMod.MouseLeave += new System.EventHandler(this.btnMod_MouseLeave);
            // 
            // btnVal
            // 
            this.btnVal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnVal.FlatAppearance.BorderSize = 0;
            this.btnVal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVal.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnVal.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.btnVal.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnVal.IconSize = 35;
            this.btnVal.Location = new System.Drawing.Point(0, 244);
            this.btnVal.Name = "btnVal";
            this.btnVal.Rotation = 0D;
            this.btnVal.Size = new System.Drawing.Size(38, 41);
            this.btnVal.TabIndex = 12;
            this.btnVal.UseVisualStyleBackColor = false;
            this.btnVal.MouseEnter += new System.EventHandler(this.btnVal_MouseEnter);
            this.btnVal.MouseLeave += new System.EventHandler(this.btnVal_MouseLeave);
            // 
            // btnAjou
            // 
            this.btnAjou.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnAjou.FlatAppearance.BorderSize = 0;
            this.btnAjou.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjou.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnAjou.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.btnAjou.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnAjou.IconSize = 35;
            this.btnAjou.Location = new System.Drawing.Point(0, 106);
            this.btnAjou.Name = "btnAjou";
            this.btnAjou.Rotation = 0D;
            this.btnAjou.Size = new System.Drawing.Size(38, 41);
            this.btnAjou.TabIndex = 9;
            this.btnAjou.UseVisualStyleBackColor = false;
            this.btnAjou.Click += new System.EventHandler(this.btnAjou_Click);
            this.btnAjou.MouseEnter += new System.EventHandler(this.btnAjou_MouseEnter);
            this.btnAjou.MouseLeave += new System.EventHandler(this.btnAjou_MouseLeave);
            // 
            // btnAnnu
            // 
            this.btnAnnu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.btnAnnu.FlatAppearance.BorderSize = 0;
            this.btnAnnu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnu.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnAnnu.IconChar = FontAwesome.Sharp.IconChar.UndoAlt;
            this.btnAnnu.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnAnnu.IconSize = 35;
            this.btnAnnu.Location = new System.Drawing.Point(0, 290);
            this.btnAnnu.Name = "btnAnnu";
            this.btnAnnu.Rotation = 0D;
            this.btnAnnu.Size = new System.Drawing.Size(38, 41);
            this.btnAnnu.TabIndex = 11;
            this.btnAnnu.UseVisualStyleBackColor = false;
            this.btnAnnu.MouseEnter += new System.EventHandler(this.btnAnnu_MouseEnter);
            this.btnAnnu.MouseLeave += new System.EventHandler(this.btnAnnu_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::AppEcole.Properties.Resources._3afbce5e_d36e_49b8_8c5b_4a0842c10084_200x200_ConvertImage;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(763, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(121, 100);
            this.panel1.TabIndex = 15;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(352, 9);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(52, 20);
            this.bunifuCustomLabel1.TabIndex = 16;
            this.bunifuCustomLabel1.Text = "Profit";
            // 
            // panelChildForm
            // 
            this.panelChildForm.Location = new System.Drawing.Point(0, 0);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(884, 562);
            this.panelChildForm.TabIndex = 17;
            // 
            // StatProfit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelChildForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StatProfit";
            this.Text = "StatProfit";
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FontAwesome.Sharp.IconButton btnCols;
        private System.Windows.Forms.Panel panel2;
        private FontAwesome.Sharp.IconButton btnSup;
        private FontAwesome.Sharp.IconButton btnMod;
        private FontAwesome.Sharp.IconButton btnVal;
        private FontAwesome.Sharp.IconButton btnAjou;
        private FontAwesome.Sharp.IconButton btnAnnu;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private FontAwesome.Sharp.IconButton iconButton21;
        private System.Windows.Forms.Panel panelChildForm;
    }
}