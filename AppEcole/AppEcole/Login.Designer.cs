﻿namespace AppEcole
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnFermer = new FontAwesome.Sharp.IconButton();
            this.txtuser = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtpass = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.btninsci = new FontAwesome.Sharp.IconButton();
            this.btnConnecter = new FontAwesome.Sharp.IconButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.picUser = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel1.Controls.Add(this.btnFermer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(336, 28);
            this.panel1.TabIndex = 8;
            // 
            // btnFermer
            // 
            this.btnFermer.FlatAppearance.BorderSize = 0;
            this.btnFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFermer.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnFermer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFermer.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.btnFermer.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.btnFermer.IconSize = 25;
            this.btnFermer.Location = new System.Drawing.Point(310, 3);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Rotation = 0D;
            this.btnFermer.Size = new System.Drawing.Size(23, 23);
            this.btnFermer.TabIndex = 18;
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            this.btnFermer.MouseEnter += new System.EventHandler(this.btnFermer_MouseEnter);
            this.btnFermer.MouseLeave += new System.EventHandler(this.btnFermer_MouseLeave);
            this.btnFermer.MouseHover += new System.EventHandler(this.btnFermer_MouseHover);
            // 
            // txtuser
            // 
            this.txtuser.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtuser.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtuser.ForeColor = System.Drawing.SystemColors.Control;
            this.txtuser.HintForeColor = System.Drawing.Color.Empty;
            this.txtuser.HintText = "";
            this.txtuser.isPassword = false;
            this.txtuser.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtuser.LineIdleColor = System.Drawing.Color.Gray;
            this.txtuser.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtuser.LineThickness = 3;
            this.txtuser.Location = new System.Drawing.Point(76, 218);
            this.txtuser.Margin = new System.Windows.Forms.Padding(4);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(189, 33);
            this.txtuser.TabIndex = 12;
            this.txtuser.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtpass
            // 
            this.txtpass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtpass.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtpass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtpass.HintForeColor = System.Drawing.Color.Empty;
            this.txtpass.HintText = "";
            this.txtpass.isPassword = true;
            this.txtpass.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtpass.LineIdleColor = System.Drawing.Color.Gray;
            this.txtpass.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtpass.LineThickness = 3;
            this.txtpass.Location = new System.Drawing.Point(76, 281);
            this.txtpass.Margin = new System.Windows.Forms.Padding(4);
            this.txtpass.Name = "txtpass";
            this.txtpass.Size = new System.Drawing.Size(189, 33);
            this.txtpass.TabIndex = 13;
            this.txtpass.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // btninsci
            // 
            this.btninsci.FlatAppearance.BorderSize = 0;
            this.btninsci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btninsci.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btninsci.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninsci.ForeColor = System.Drawing.Color.White;
            this.btninsci.IconChar = FontAwesome.Sharp.IconChar.UserPlus;
            this.btninsci.IconColor = System.Drawing.Color.Gray;
            this.btninsci.IconSize = 30;
            this.btninsci.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btninsci.Location = new System.Drawing.Point(169, 363);
            this.btninsci.Name = "btninsci";
            this.btninsci.Rotation = 0D;
            this.btninsci.Size = new System.Drawing.Size(124, 27);
            this.btninsci.TabIndex = 17;
            this.btninsci.Text = "S\'inscrire";
            this.btninsci.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btninsci.UseVisualStyleBackColor = true;
            this.btninsci.MouseEnter += new System.EventHandler(this.btninsci_MouseEnter);
            this.btninsci.MouseLeave += new System.EventHandler(this.btninsci_MouseLeave);
            // 
            // btnConnecter
            // 
            this.btnConnecter.FlatAppearance.BorderSize = 0;
            this.btnConnecter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnecter.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnConnecter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnecter.ForeColor = System.Drawing.Color.White;
            this.btnConnecter.IconChar = FontAwesome.Sharp.IconChar.SignInAlt;
            this.btnConnecter.IconColor = System.Drawing.Color.Gray;
            this.btnConnecter.IconSize = 30;
            this.btnConnecter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnecter.Location = new System.Drawing.Point(28, 363);
            this.btnConnecter.Name = "btnConnecter";
            this.btnConnecter.Rotation = 0D;
            this.btnConnecter.Size = new System.Drawing.Size(124, 27);
            this.btnConnecter.TabIndex = 16;
            this.btnConnecter.Text = "Connecter";
            this.btnConnecter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConnecter.UseVisualStyleBackColor = true;
            this.btnConnecter.Click += new System.EventHandler(this.btnConnecter_Click);
            this.btnConnecter.MouseEnter += new System.EventHandler(this.btnConnecter_MouseEnter);
            this.btnConnecter.MouseLeave += new System.EventHandler(this.btnConnecter_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::AppEcole.Properties.Resources.Secure_50px;
            this.pictureBox2.Location = new System.Drawing.Point(44, 281);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(34, 33);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // picUser
            // 
            this.picUser.Image = global::AppEcole.Properties.Resources.User_Male_50px;
            this.picUser.Location = new System.Drawing.Point(44, 218);
            this.picUser.Name = "picUser";
            this.picUser.Size = new System.Drawing.Size(34, 33);
            this.picUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUser.TabIndex = 14;
            this.picUser.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(94, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(139, 122);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.panel1;
            this.bunifuDragControl1.Vertical = true;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.ClientSize = new System.Drawing.Size(336, 497);
            this.Controls.Add(this.btninsci);
            this.Controls.Add(this.btnConnecter);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.picUser);
            this.Controls.Add(this.txtpass);
            this.Controls.Add(this.txtuser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.Opacity = 0.85D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtuser;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtpass;
        private System.Windows.Forms.PictureBox picUser;
        private System.Windows.Forms.PictureBox pictureBox2;
        private FontAwesome.Sharp.IconButton btnConnecter;
        private FontAwesome.Sharp.IconButton btninsci;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private FontAwesome.Sharp.IconButton btnFermer;
    }
}